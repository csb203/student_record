export const MARK_LIST_ADDRESS = "0x3d85d6632f681Cc75a7d8EC976254BdFcfB24fB4"

export const MARK_LIST_ABI =[
    {
      "inputs": [],
      "stateMutability": "nonpayable",
      "type": "constructor"
    },
    {
      "anonymous": false,
      "inputs": [
        {
          "indexed": true,
          "internalType": "uint256",
          "name": "cid",
          "type": "uint256"
        },
        {
          "indexed": true,
          "internalType": "string",
          "name": "code",
          "type": "string"
        },
        {
          "indexed": false,
          "internalType": "enum MarkList.Grades",
          "name": "grades",
          "type": "uint8"
        }
      ],
      "name": "addMarksEvent",
      "type": "event"
    },
    {
      "anonymous": false,
      "inputs": [
        {
          "indexed": true,
          "internalType": "uint256",
          "name": "cid",
          "type": "uint256"
        },
        {
          "indexed": true,
          "internalType": "string",
          "name": "code",
          "type": "string"
        },
        {
          "indexed": false,
          "internalType": "enum MarkList.Grades",
          "name": "grade",
          "type": "uint8"
        }
      ],
      "name": "updateMarksEvent",
      "type": "event"
    },
    {
      "inputs": [
        {
          "internalType": "uint256",
          "name": "",
          "type": "uint256"
        },
        {
          "internalType": "string",
          "name": "",
          "type": "string"
        }
      ],
      "name": "marks",
      "outputs": [
        {
          "internalType": "uint256",
          "name": "cid",
          "type": "uint256"
        },
        {
          "internalType": "string",
          "name": "code",
          "type": "string"
        },
        {
          "internalType": "enum MarkList.Grades",
          "name": "grade",
          "type": "uint8"
        }
      ],
      "stateMutability": "view",
      "type": "function",
      "constant": true
    },
    {
      "inputs": [],
      "name": "marksCount",
      "outputs": [
        {
          "internalType": "uint256",
          "name": "",
          "type": "uint256"
        }
      ],
      "stateMutability": "view",
      "type": "function",
      "constant": true
    },
    {
      "inputs": [
        {
          "internalType": "uint256",
          "name": "_cid",
          "type": "uint256"
        },
        {
          "internalType": "string",
          "name": "_code",
          "type": "string"
        },
        {
          "internalType": "enum MarkList.Grades",
          "name": "_grade",
          "type": "uint8"
        }
      ],
      "name": "addMarks",
      "outputs": [],
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "inputs": [
        {
          "internalType": "uint256",
          "name": "_cid",
          "type": "uint256"
        },
        {
          "internalType": "string",
          "name": "_code",
          "type": "string"
        }
      ],
      "name": "findMarks",
      "outputs": [
        {
          "components": [
            {
              "internalType": "uint256",
              "name": "cid",
              "type": "uint256"
            },
            {
              "internalType": "string",
              "name": "code",
              "type": "string"
            },
            {
              "internalType": "enum MarkList.Grades",
              "name": "grade",
              "type": "uint8"
            }
          ],
          "internalType": "struct MarkList.Marks",
          "name": "",
          "type": "tuple"
        }
      ],
      "stateMutability": "view",
      "type": "function",
      "constant": true
    },
    {
      "inputs": [
        {
          "internalType": "uint256",
          "name": "_cid",
          "type": "uint256"
        },
        {
          "internalType": "string",
          "name": "_code",
          "type": "string"
        },
        {
          "internalType": "enum MarkList.Grades",
          "name": "_grade",
          "type": "uint8"
        }
      ],
      "name": "updateMarks",
      "outputs": [
        {
          "components": [
            {
              "internalType": "uint256",
              "name": "cid",
              "type": "uint256"
            },
            {
              "internalType": "string",
              "name": "code",
              "type": "string"
            },
            {
              "internalType": "enum MarkList.Grades",
              "name": "grade",
              "type": "uint8"
            }
          ],
          "internalType": "struct MarkList.Marks",
          "name": "",
          "type": "tuple"
        }
      ],
      "stateMutability": "nonpayable",
      "type": "function"
    }
  ]