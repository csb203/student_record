import React, { Component } from "react"
class SubjectList extends Component {
    render() {
        return (
            <div className="container" style={{display:"flex", justifyContent:"space-evenly"}}>
                <div>
                <form className="p-5 border" onSubmit={(event) => {
                    event.preventDefault()
                    this.props.createSubject(this.code.value, this.subject.value)
                }}>

                    <h2>Add Subject</h2>

                    <div className="form-group mb-3">
                        <label htmlFor="exampleInputCid">CODE</label>
                        <input type="text" className="form-control" id="newCODE" ref={(input) => { this.code = input; }} aria-describedby="CIDHelp" placeholder="Enter CODE" required />
                    </div>
                    <div className="form-group mb-3">
                        <label htmlFor="exampleInputStudentName">Subject Name</label>
                        <input type="text" className="form-control" id="newSubject" ref={(input) => { this.subject = input; }} placeholder="Subject Name" required />
                    </div>
                    <input className="form-control btn btn-success" type="submit" hidden="" />
                </form>
                </div>
                <div>
                <div>                
                    <form className="p-5 border" onSubmit={(event) => {
                    event.preventDefault()
                    this.props.updateSubject(this.id.value, this.code2.value, this.subject2.value)
                }}>

                    <h2>Update Subject</h2>
                    <div className="form-group mb-3">
                        <label htmlFor="exampleInputCid">ID</label>
                        <input type="text" className="form-control" id="newCID" ref={(input) => { this.id = input; }} aria-describedby="CIDHelp" placeholder="Enter ID" required />
                    </div>
                    <div className="form-group mb-3">
                        <label htmlFor="exampleInputCid">CODE</label>
                        <input type="text" className="form-control" id="newCODE" ref={(input) => { this.code2 = input; }} aria-describedby="CIDHelp" placeholder="Enter CODE" required />
                    </div>
                    <div className="form-group mb-3">
                        <label htmlFor="exampleInputStudentName">Subject Name</label>
                        <input type="text" className="form-control" id="newSubject" ref={(input) => { this.subject2 = input; }} placeholder="Subject Name" required />
                    </div>
                    <input className="form-control btn btn-success" type="submit" hidden="" />
                </form>
                </div>
                </div>
            </div>
        )
    }
}

export default SubjectList;