import React, { Component } from "react"
class MarkList extends Component {

    constructor(props){
        super(props)
        this.state ={
            marks:"",
        }
    }
    render() {
        return (
            <div className="container">
                <form className="p-5 border" onSubmit={(event) =>{
                    event.preventDefault()
                    this.props.addMarks(this.studentid.value,this.subjectid.value, this.grade.value)

                }}>

                    <h2>Add Mark</h2>
                    <select id="studentid" className="form-select mb-2" ref={(input) =>{this.studentid =input}}><option defaultValue={true}>Open this select menu</option>
                    {this.props.students.map((student,key) =>{
                        return (
                            <option key={key} value={student.cid}>{student.cid}{student.name}</option>
                        )
                    })}</select>
                    
                    <select id="subjectid" className="form-select mb-2" ref={(input) =>{this.subjectid =input}}><option defaultValue={true}>Open this select menu</option>
                    {this.props.subjects.map((subject,key) =>{
                        return (
                            <option key={key} value={subject.code}>{subject.code}{subject.name}</option>
                        )
                    })}</select>
                    
                    <select id="grades" className="form-select mb-2" ref={(input) =>{this.grade =input}}><option defaultValue={true}>Open this select menu</option>
                    <option value="1">A</option>
                    <option value="2">B</option>
                    <option value="3">C</option>
                    <option value="4">D</option>
                    <option value="5">Fail</option>
                  </select>
                    <input className="form-control btn btn-success" type="submit" hidden="" />
                </form>
                <form className="p-5 border" onSubmit={(event) =>{
                    event.preventDefault()
                    this.props.findMarks(this.studentid.value,this.subjectid.value)
                    .then((result) =>{
                        this.setState({marks: result.grade})
                        console.log(result.grade)
                    })
                }}>

                    <h2>Find Mark</h2>
                    <select id="studentid" className="form-select mb-2" ref={(input) =>{this.studentid =input}}><option defaultValue={true}>Open this select menu</option>
                    {this.props.students.map((student,key) =>{
                        return (
                            <option key={key} value={student.cid}>{student.cid}{student.name}</option>
                        )
                    })}</select>
                    
                    <select id="subjectid" className="form-select mb-2" ref={(input) =>{this.subjectid =input}}><option defaultValue={true}>Open this select menu</option>
                    {this.props.subjects.map((subject,key) =>{
                        return (
                            <option key={key} value={subject.code}>{subject.code}{subject.name}</option>
                        )
                    })}</select>
                    <input className="form-control btn btn-success" type="submit" hidden="" />
                    {console.log(this.state.marks)}
                    <p>Grade:{this.state.marks}</p>
                </form>

            </div>
        )
    }
}

export default MarkList;