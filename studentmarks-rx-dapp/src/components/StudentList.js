import React, { Component } from "react"
class StudentList extends Component {
    render() {
        return (
            <div className="container1" style={{display:"flex", justifyContent:"space-evenly"}}>
                <div>
                    <form className="p-5 border" onSubmit={(event) => {
                        event.preventDefault()
                        console.log(this.cid1.value)
                        console.log(this.student1.value)
                        this.props.createStudent(this.cid1.value, this.student1.value)
                    }}>

                        <h2>Add Student</h2>

                        <div className="form-group mb-3">
                            <label htmlFor="exampleInputCid">CID</label>
                            <input type="text" className="form-control" id="newCID" ref={(input) => { this.cid1 = input; }} aria-describedby="CIDHelp" placeholder="Enter CID" required />
                        </div>
                        <div className="form-group mb-3">
                            <label htmlFor="exampleInputStudentName">Student Name</label>
                            <input type="text" className="form-control" id="newStudent" ref={(input) => { this.student1 = input; }} placeholder="Student Name" required />
                        </div>
                        <input className="form-control btn btn-success" type="submit" hidden="" />
                    </form>
                </div>
                <div>
                    <div>
                        <form className="p-5 ml-5 border" onSubmit={(event) => {
                            event.preventDefault()
                            this.props.updateStudent(this.id.value, this.cid.value, this.student.value)
                        }}>

                            <h2>Update Student</h2>

                            <div className="form-group mb-3">
                                <label htmlFor="exampleInputCid">ID</label>
                                <input type="text" className="form-control" id="newCID" ref={(input) => { this.id = input; }} aria-describedby="CIDHelp" placeholder="Enter ID" required />
                            </div>
                            <div className="form-group mb-3">
                                <label htmlFor="exampleInputCid">CID</label>
                                <input type="text" className="form-control" id="newCID" ref={(input) => { this.cid = input; }} aria-describedby="CIDHelp" placeholder="Enter CID" required />
                            </div>
                            <div className="form-group mb-3">
                                <label htmlFor="exampleInputStudentName">Student Name</label>
                                <input type="text" className="form-control" id="newStudent" ref={(input) => { this.student = input; }} placeholder="Student Name" required />
                            </div>
                            <input className="form-control btn btn-success" type="submit" hidden="" />
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

export default StudentList;