// step2: change the base code to use component
import React, { Component } from 'react'
import Web3 from "web3"
import './App.css';
import StudentList from './components/StudentList';
import SubjectList from './components/SubjectList';
import MarkList from './components/MarkList';
// step3
import { STUDENT_LIST_ABI, STUDENT_LIST_ADDRESS } from './abi/config_studentList';
import { SUBJECT_LIST_ABI, SUBJECT_LIST_ADDRESS } from './abi/config_subjectList';
import { MARK_LIST_ABI, MARK_LIST_ADDRESS } from "./abi/config_markList";

class App extends Component {

  componentDidMount() {
    if (!window.ethereum)
      throw new Error("No crypto wallet found. Please install it.");
    window.ethereum.send("eth_requestAccounts");
    this.loadBlockchainData();
  }
  async loadBlockchainData() {
    const web3 = new Web3(Web3.givenProvider || "https://sepolia.infura.io/v3/887efde57c8b4c2e8b666c19e6178587")
    const accounts = await web3.eth.getAccounts()
    this.setState({ account: accounts[0] })
    const studentList = new web3.eth.Contract(
      STUDENT_LIST_ABI, STUDENT_LIST_ADDRESS
    )
    const subjectList = new web3.eth.Contract(
      SUBJECT_LIST_ABI, SUBJECT_LIST_ADDRESS
    )
    const markList = new web3.eth.Contract(
      MARK_LIST_ABI, MARK_LIST_ADDRESS
    )
    this.setState({ markList })
    this.setState({ subjectList })
    this.setState({ studentList })
    const markCount = await markList.methods.marksCount().call()
    const subjectCount = await subjectList.methods.subjectsCount().call()
    const studentCount = await studentList.methods.studentsCount().call()
    this.setState({ markCount })
    this.setState({ subjectCount })
    this.setState({ studentCount })
    this.setState({ marks: [] });
    this.setState({ students: [] });
    this.setState({ subjects: [] });
    for (var j = 1; j <= subjectCount; j++) {
      const subject = await subjectList.methods.subjects(j).call()
      this.setState({ subjects: [...this.state.subjects, subject] })
    }
    for (var i = 1; i <= studentCount; i++) {
      const student = await studentList.methods.students(i).call()
      this.setState({ students: [...this.state.students, student] })
    }
  }
  // initalize the variables stored in the state
  constructor(props) {
    super(props)
    this.state = {
      account: "",
      studentCount: 0,
      students: [],
      subjectCount: 0,
      subjects: [],
      markCount: 0,
      marks: [],
      loading: true,
    }
    this.createStudent = this.createStudent.bind(this)
    this.updateStudent = this.updateStudent.bind(this)
    this.markGraduated = this.markGraduated.bind(this)
    this.createSubject = this.createSubject.bind(this)
    this.updateSubject = this.updateSubject.bind(this)
    this.markRetired = this.markRetired.bind(this)
    this.addMarks = this.addMarks.bind(this)
    this.findMarks =this.findMarks.bind(this)

  }
  addMarks(studentid, subjectid, grade) {
    this.setState({ loading: true })
    this.state.markList.methods.addMarks(studentid, subjectid, grade)
      .send({ from: this.state.account })
      .once("receipt", (receipt) => {
        this.setState({ loading: false })
        this.loadBloackchainData()
      })
  }
  findMarks(studentid,subjectid){
    this.setState({loading:true})
    return this.state.markList.methods.findMarks(studentid,subjectid)
      .call({from:this.state.account})
  }
  createSubject(code, name) {
    this.setState({ loading: true })
    this.state.subjectList.methods.createSubject(code, name)
      .send({ from: this.state.account })
      .once("receipt", (receipt) => {
        this.setState({ loading: false })
        this.loadBlockchainData()
      })
  }
  updateSubject(id ,code ,name){
    this.setState({ loading: true })
    this.state.subjectList.methods.updateSubject(id, code, name)
      .send({ from: this.state.account })
      .once("receipt", (receipt) => {
        this.setState({ loading: false })
        this.loadBlockchainData()
      })
  }
  createStudent(cid, name) {
    this.setState({ loading: true })
    this.state.studentList.methods.createStudent(cid, name)
      .send({ from: this.state.account })
      .once("receipt", (receipt) => {
        this.setState({ loading: false })
        this.loadBlockchainData()
      })
  }

  markGraduated(cid) {
    this.setState({ loading: true })
    this.state.studentList.methods.markGraduated(cid)
      .send({ from: this.state.account })
      .once("receipt", (receipt) => {
        this.setState({ loading: false })
        this.loadBlockchainData()
      })
  }
  // function to update a student's details
  updateStudent(id, cid, name) {
    console.log(id, cid, name)
    this.setState({ loading: true })
    this.state.studentList.methods.updateStudent(id, cid, name)
      .send({ from: this.state.account })
      .once("receipt", (receipt) => {
        this.setState({ loading: false })
        this.loadBlockchainData()
      })
  }
  markRetired(cid) {
    this.setState({ loading: true })
    this.state.subjectList.methods.markretired(cid)
      .send({ from: this.state.account })
      .once("receipt", (receipt) => {
        this.setState({ loading: false })
        this.loadBlockchainData()
      })
  }
  render() {
    return (
      <div className='container'>
        <h1>Student Marks Management System</h1>
        <p>Your account:{this.state.account}</p>
        <ul id='studentList' className='list-unstyled'>
          <h4>Total Students Records: {this.state.studentCount}</h4>
          <StudentList
            createStudent={this.createStudent}
            updateStudent={this.updateStudent} />

          {
            // This gets the each student from the studentList and pass them into a function that display the details of student
            this.state.students.map((student, key) => {
              return (
                <li className='list-group-item checkbox' key={key}>
                  <span className='name alert'>{student._id} {student.cid} {student.name}</span>
                  <input className='form-check-input' type='checkbox' name={student._id} defaultChecked={student.graduated} disabled={student.graduated} ref={(input) => {
                    this.checkbox = input
                  }}
                    onClick={(event) => {
                      this.markGraduated(event.currentTarget.name)
                    }}
                  />
                  <label className='form-check-label'>Graduated</label>
                  {/* <div>
                    <button onClick={() => this.updateStudent(student._id,student.cid,student.name)}>Update</button>
                  </div> */}
                </li>
              )
            })
          }
        </ul>
        <ul id='subjectList' className='list-unstyled'>
          <h4>Total Subjects Records: {this.state.subjectCount}</h4>
          {<SubjectList
            createSubject={this.createSubject}
            updateSubject={this.updateSubject} />}
          {
            this.state.subjects.map((subject, key) => {
              return (
                <li className='list-group-item checkbox' key={key}>
                  <span className='name alert'>{subject._id} {subject.code} {subject.name}</span>
                  <input className='form-check-input' type='checkbox' name={subject._id} defaultChecked={subject.retired} disabled={subject.retired} ref={(input) => {
                    this.checkbox = input
                  }}
                    onClick={(event) => {
                      this.markRetired(event.currentTarget.name)
                    }}
                  />
                  <label className='form-check-label'>Retired</label>
                </li>

              )
            })
          }
        </ul>
        <ul id='markList' className='list-unstyled'>
          <h4>Total Marks Record: {this.state.markCount}</h4>
          {<MarkList
            subjects={this.state.subjects}
            students={this.state.students}
            findMarks={this.findMarks}  
            addMarks={this.addMarks} />}
        
        </ul>
      </div>
    )
  }
}
export default App;