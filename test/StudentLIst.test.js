//Import the StudentList smart Contract
const StudentList =artifacts.require("StudentList");

contract ("StudentList",(account) =>{
    beforeEach(async () => {
    //Make sure contact is deployed and before we retrive the studentList for testing
        this.studentList =await StudentList.deployed()
    })

    it("deploys successfully", async () =>{
        //Get the address which the student object is stored
        const address =await this.studentList.address
        //Test for valid address
        isValidAddress(address)
    })

    //Testing the content in the contract
    it("test adding students",async () => {
        return StudentList.deployed().then ((instance) => {
            s= instance;
            studentCid =1;
            return s.createStudent(studentCid, "Ugyen Lhendup");
        }).then((transaction) => {
            isValidAddress(transaction.tx);
            isValidAddress(transaction.receipt.blockHash);
            return s.studentsCount()
        }).then((count) => {
            assert.equal(count,1)
            return s.students(1);
        }).then((student) =>{
            assert.equal(student.cid,studentCid)
        })
    })

    //Testing the content in the contract
    it("test finding students",async () => {
        return StudentList.deployed().then(async(instance) =>{
            s=instance;
            studentCid =2;
            return s.createStudent(studentCid++,"Tshering Yangzom").then(async (tx)=>{
            return s.createStudent(studentCid++,"Dorji Tshering").then(async(tx) =>{
            return s.createStudent(studentCid++,"Chimi Dema").then(async(tx) =>{
            return s.createStudent(studentCid++,"Tashi Phuntsho").then(async(tx) =>{
            return s.createStudent(studentCid++,"Samten Zangmo").then(async(tx) =>{
            return s.createStudent(studentCid++,"Sonam Chophel").then(async(tx) =>{

                return s.studentsCount().then(async(count) =>{
                    assert.equal(count,7)
                    return s.findStudent(5).then(async(student) =>{
                        assert.equal(student.name,"Tashi Phuntsho")
                    })
                })
            

            })
        }) 
    
    })
    })
    })
    })
    })
    })

    it("test mark graduate students", async() =>{
        return StudentList.deployed().then(async(instance) =>{
            s= instance;
            return s.findStudent(1).then(async(ostudent) =>{
                assert.equal(ostudent.name,"Ugyen Lhendup")
                assert.equal(ostudent.graduated,false)
                return s.markGraduated(1).then(async(transaction) =>{
                    return s.findStudent(1).then(async(nstudent) =>{
                        assert.equal(nstudent.name,"Ugyen Lhendup")
                        assert.equal(nstudent.graduated,true)
                        return
                    })
                })
            })
        })
    })
})

//This function check if the address is valid

function isValidAddress(address){
        assert.notEqual(address,0x0)
        assert.notEqual(address," ")
        assert.notEqual(address,null)
        assert.notEqual(address,undefined)
        
}
