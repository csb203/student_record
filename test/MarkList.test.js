// Import the MarkList smart contract
const MarkList = artifacts.require("MarkList");
const SubjectList = artifacts.require("SubjectList");
const StudentList = artifacts.require("StudentList");
const Grades = {
    A:0,
    B:1,
    C:2,
    D:3,
    FAIL:4
};


// Start of test 
contract("MarkList", accounts => {
  beforeEach(async () => {
    this.MarkList = await MarkList.deployed();
  });

  // Test case to check the name of the contract
  it("testing contract name'", async () => {
    const MarkListName =await MarkList.contractName;
    const StudentListName =await StudentList.contractName;
    const SubjectListName = await SubjectList.contractName;
    assert.equal(MarkListName, "MarkList");
    assert.equal(StudentListName, "StudentList");
    assert.equal(SubjectListName, "SubjectList");
  });

  it("deploys successfully",async() =>{
    const address = await this.MarkList.address
    isValidAddress(address);
  })

  it("adding students", async () => {
    const s = await StudentList.deployed();
    const numStudents = 5;
    const students = [];
    const keyandvalue= {
      "Ugyen Lhendup": 1,
      "Tshering Yangzom": 2,
      "Dorji Tshering": 3,
      "Chimi Dema": 4,
      "Tashi Phuntsho": 5,
      "Samten Zangmo": 6,
      "Sonam Chophel": 7
    };
  
    for (let i = 0; i < numStudents; i++) {
      const studentName = ["Ugyen Lhendup", "Tshering Yangzom", "Dorji Tshering", "Chimi Dema", "Tashi Phuntsho", "Samten Zangmo", "Sonam Chophel"][i];
      const studentCid = keyandvalue[studentName];
  
      await s.createStudent(studentCid, studentName);
      students.push({ studentCid, studentName });
    }
    
    return s.findStudent(3).then(async(student) =>{
      assert.equal(student.name,"Dorji Tshering")
  })
  });
  // it("test adding marks", async () =>{
  //   return MarkList.deployed().then((instance) =>{
  //       s =instance;
  //       Grade = Grades.A;
  //       return s.addMarks(1,"CSB203",Grade);   
  // }).then((txHash) =>{
  //   assert.equal(Grade,Grades.A)
  // })
    it("test adding marks", async () =>{
    return MarkList.deployed().then((instance) =>{
        s =instance;
        Grade = Grades.A;
        return s.addMarks(1,"CSB203",Grade);   
  }).then((txHash) =>{
    assert.equal(Grade,Grades.A)
  })
  
})
it("test updating marks",async () => {
      await MarkList.deployed().then(async(instance) =>{
      s=instance;
      const cid =1;
      const code ="ELE201";
      return s.updateMarks(cid,code,Grades.A).then(async (tx)=>{
        console.log(s.updateMarks)
              return s.findMarks(cid,code).then(async(marks) =>{
        console.log(s.findMarks)
                  assert.equal(marks.grade,Grades.A)
              })
          })
})
})
});


function isValidAddress(address) {
    // assert.equal(address.length, 42, "Address length should be 42");
    assert.equal(address.slice(0, 2), "0x", "Address should start with '0x'");
}
