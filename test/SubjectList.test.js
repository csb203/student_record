//Import the SubjectList smart Contract
const SubjectList =artifacts.require("SubjectList");

contract ("SubjectList",(account) =>{
    beforeEach(async () => {
        this.subjectList =await SubjectList.deployed()
    })

    it("deploys successfully", async () =>{
        //Get the address which the subject object is stored
        const address =await this.subjectList.address
        //Test for valid address
        isValidAddress(address)
    })

    it("test adding subjects",async() => {
        return SubjectList.deployed().then((instance) =>{
            s =instance;
            subjectCode ="203";
            return s.createSubject(subjectCode,"Developing Dapp");
        }).then((transaction) =>{
            isValidAddress(transaction.tx);
            isValidAddress(transaction.receipt.blockHash);
            return s.subjectsCount()
        }).then((count) => {
            assert.equal(count,1)
            return s.subjects(1);
        }).then((subject) =>{
            
            assert.equal(subject.code,subjectCode)
        })

    })
    
    //Testing the content in the contract
    it("test finding subjects",async () => {
        return SubjectList.deployed().then(async(instance) =>{
            s=instance;
            return s.createSubject("CSB203","Developing Dapp2").then(async (tx)=>{
            return s.createSubject("CSB204","Developing Dapp3").then(async(tx) =>{
            return s.createSubject("CSB205","Developing Dapp4").then(async(tx) =>{
            return s.createSubject("CSB206","Developing Dapp5").then(async(tx) =>{
            return s.createSubject("CSB207","Developing Dapp6").then(async(tx) =>{
            return s.createSubject("CSB208","Developing Dapp7").then(async(tx) =>{

                return s.subjectsCount().then(async(count) =>{
                    assert.equal(count,7)
                    return s.findSubject(5).then(async(subject) =>{
                        assert.equal(subject.name,"Developing Dapp5")
                    })
                })
            

            })
        }) 
    
    })
    })
    })
    })
    })
    })
     
    //Testing the content in the contract
    it("test updating subjects",async () => {
        return SubjectList.deployed().then(async(instance) =>{
            s=instance;
            return s.updateSubject(5,"ELE201","Cyber Growth Conversation").then(async (tx)=>{
                    return s.findSubject(5).then(async(subject) =>{
                        assert.equal(subject.code,"ELE201")
                        assert.equal(subject.name,"Cyber Growth Conversation")
                    })
                })
    })
    })
    })
//This function check if the address is valid

function isValidAddress(address) {
    // assert.equal(address.length, 42, "Address length should be 42");
    assert.equal(address.slice(0, 2), "0x", "Address should start with '0x'");
}