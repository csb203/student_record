//SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

contract StudentList {
    uint public studentsCount = 0;

    //model a student

    struct Student {
        uint _id;
        uint cid;
        string name;
        bool graduated;
    }
    mapping(uint => Student) public students;

    //Constructor for Students
    constructor() {
        // createStudent(1001, "Tshewang Paljor Wangchuk");
    }

    //Events
    event createStudentEvent(
        uint __id,
        uint indexed cid,
        string name,
        bool graduated
    );
    event updateStudentEvent(uint _id, uint indexed _cid, string _name);
    //Event for graduation status
    event markGraduatedEvent(uint indexed cid);

    function markGraduated(uint _id) public returns (Student memory) {
        students[_id].graduated = true;
        emit markGraduatedEvent(_id);
        return students[_id];
    }

    //Fetch student info from the storage
    function findStudent(uint _id) public view returns (Student memory) {
        return students[_id];
    }

    //create and add student to storage
    function createStudent(
        uint _studentCid,
        string memory _name
    ) public returns (Student memory) {
        studentsCount++;
        students[studentsCount] = Student(
            studentsCount,
            _studentCid,
            _name,
            false
        );
        //Trigger create event
        emit createStudentEvent(studentsCount, _studentCid, _name, false);
        return students[studentsCount];
    }

    function updateStudent(
        uint _id,
        uint _cid,
        string memory _name
    ) public returns (Student memory) {
        students[_id].cid = _cid;
        students[_id].name = _name;

        emit updateStudentEvent(_id, _cid,_name);
        return students[_id];
    }
}
