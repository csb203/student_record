// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

contract SubjectList {
    uint public subjectsCount = 0;

    struct Subject {
        uint _id;
        string code;
        string name;
        bool retired;
    }

    mapping(uint => Subject) public subjects;

    constructor() {}

    event createSubjectEvent(
        uint _id,
        string _code,
        string _name,
        bool retired
    );

    event markretiredEvent(uint indexed _id);
    event updateSubjectEvent(uint _id, string _code, string _name);

    function createSubject(
        string memory _code,
        string memory _name
    ) public returns (Subject memory) {
        subjectsCount++;
        subjects[subjectsCount] = Subject(subjectsCount, _code, _name, false);

        emit createSubjectEvent(subjectsCount, _code, _name, false);
        return subjects[subjectsCount];
    }

    function markretired(uint _id) public returns (Subject memory) {
        subjects[_id].retired = true;
        emit markretiredEvent(_id);
        return subjects[_id];
    }

    function findSubject(uint _id) public view returns (Subject memory) {
        return subjects[_id];
    }

    function updateSubject(
        uint _id,
        string memory _code,
        string memory _name
    ) public returns (Subject memory) {
        subjects[_id].code = _code;
        subjects[_id].name = _name;

        emit updateSubjectEvent(_id, _code, _name);
        return subjects[_id];
    }
}
